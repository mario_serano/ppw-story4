from django.shortcuts import render

# Create your views here.
content = [
    {
        "slug":"COMPFEST",
        "position":"Frontend Developer",
        "paragraphs":[
            "The biggest IT event in Indonesia held by student",
            "Helped code the frontend of the website, using React, and Gatsby. Experienced in handling data transfer between API and Client using Redux and Axios",
            "Made the whole Applications Dashboard page, using Redux, Contentful, and Gatsby. Created reusable components such as Accordions, Carousel, and many more."
        ],
    },
    {
        "slug":"CEDSUI",
        "position":"Supervisor of Entrepreneurship Community and Startup Weekend UI",
        "paragraphs":[
            "The biggest Entrepreneurship Community in UI",
            "Had a lot of experience through listening to mentors, speakers, and other amazing people there",
            "Made me inspired to create, and be impactful in the community"
        ]
    },
    {
        "slug":"BETIS",
        "position":"Curriculum and Academic Managements",
        "paragraphs":[
            "Teached English in one of the class in BETIS",
            "Volunteered my way as a guy that loves teaching science and education in general",
            "Learned Sociology, which I've never learned and fascinated by the idea"
        ],
    },
    {
        "slug":"PERAK",
        "position":"Games",
        "paragraphs":{
            "Had fun and parted ways with games for the last time",
            "I really loved games, Dota 2 was one of my favorite game of all time, had over 2k+ hours and 4k MMR",
            "Reality needs to be faced, and you need to let go"
        }
    }
]
content_fourth_section = [
    {
        "slug":"STREAMLINE",
        "paragraphs":[
            "An app made for hackathon, in UGM.",
            "Idea is to make logistics easier, using Machine Learning",
            "Got to the finals with our proposal, and gaining 11th place at the competition"
        ]
    },
    {
        "slug":"SEKELAS",
        "paragraphs":[
            "Freelanced at sekelas, using React and Django",
            "Learned a lot, including react redux, nextjs, and SEO optimization"
        ]
    }
]
def index(request):
    context = {
        "content_section_three":content,
        "content_section_four":content_fourth_section
    }
    return render(request, "story3/index.html", context)

def about(request):
    return render(request, "story3/about.html")

def experience(request):
    context = {
        "content_section_three":content
    }
    return render(request, "story3/experience.html", context)

def projects(request):
    context = {
        "content_section_four":content_fourth_section
    }
    return render(request, "story3/projects.html", context)