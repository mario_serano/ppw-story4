from django.test import TestCase, Client
from django.urls import resolve
from . import views

# Create your tests here.
class Story7Test(TestCase):

    def test_story7_index_url_is_exist(self):
        response = Client().get('/story-7/') 
        self.assertEqual(response.status_code, 200)
    
    def test_story7_index_using_index_template(self):
        response = Client().get('/story-7/')
        self.assertTemplateUsed(response, 'story7/index.html')

    def test_story7_using_index_func(self):
        found = resolve('/story-7/')
        self.assertEqual(found.func, views.index)