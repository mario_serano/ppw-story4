let acc = $(".accordion-big");

for(let i = 1; i <= acc.length; i++){
    $(`#accordion-${i}`).click((event) => {
        let className = event.target.className;
        event.target.className = !className.split(" ")[1] ? className += " active" : "accordion";
        
        let curHeight = $("#section-" + i)[0].style.maxHeight
        $("#section-" + i)[0].style.maxHeight = curHeight === "0px" ? "100%" : "0px" 
    })
    $("#up-" + i).click((event) => {
        if(i > 1){
            let swapHeadNode1 = $(`#accordion-${i-1}`)
            let swapHeadNode2 = $(`#accordion-${i}`)

            let cloneHeadNode1 = swapHeadNode1.clone()
            let cloneHeadNode2 = swapHeadNode2.clone()

            cloneHeadNode1[0].id = `accordion-${i}`
            cloneHeadNode2[0].id = `accordion-${i-1}`

            cloneHeadNode1.click((event) => {
                let className = event.target.className;
                event.target.className = !className.split(" ")[1] ? className += " active" : "accordion";
                
                let curHeight = $("#section-" + i)[0].style.maxHeight
                $("#section-" + i)[0].style.maxHeight = curHeight === "0px" ? "100%" : "0px" 
            })

            cloneHeadNode2.click((event) => {
                let className = event.target.className;
                event.target.className = !className.split(" ")[1] ? className += " active" : "accordion";
                
                let curHeight = $(".panel")[i-2].style.maxHeight
                $(".panel")[i-2].style.maxHeight = curHeight === "0px" ? "100%" : "0px"
            })

            $(".accordion-big")[i-1].replaceChild(cloneHeadNode1[0], swapHeadNode2[0])
            $(".accordion-big")[i-2].replaceChild(cloneHeadNode2[0], swapHeadNode1[0])

            let swapBottomNode1 = $(`#section-${i-1}`)
            let swapBottomNode2 = $(`#section-${i}`)  

            let cloneBottomNode1 = swapBottomNode1.clone()
            let cloneBottomNode2 = swapBottomNode2.clone()

            cloneBottomNode1[0].id = `section-${i}`
            cloneBottomNode2[0].id = `section-${i-1}`

            $(".accordion-big")[i-1].replaceChild(cloneBottomNode1[0], swapBottomNode2[0])
            $(".accordion-big")[i-2].replaceChild(cloneBottomNode2[0], swapBottomNode1[0])

        }           
    })
    $("#down-" + i).click((event) => {
        if(i < acc.length){
            let swapHeadNode1 = $(`#accordion-${i}`)
            let swapHeadNode2 = $(`#accordion-${i+1}`)

            let cloneHeadNode1 = swapHeadNode1.clone()
            let cloneHeadNode2 = swapHeadNode2.clone()

            cloneHeadNode1[0].id = `accordion-${i+1}`
            cloneHeadNode2[0].id = `accordion-${i}`

            $(".accordion-big")[i].replaceChild(cloneHeadNode1[0], swapHeadNode2[0])
            $(".accordion-big")[i-1].replaceChild(cloneHeadNode2[0], swapHeadNode1[0])

            let swapBottomNode1 = $(`#section-${i}`)
            let swapBottomNode2 = $(`#section-${i+1}`)  

            let cloneBottomNode1 = swapBottomNode1.clone()
            let cloneBottomNode2 = swapBottomNode2.clone()

            cloneBottomNode1[0].id = `section-${i+1}`
            cloneBottomNode2[0].id = `section-${i}`

            $(".accordion-big")[i].replaceChild(cloneBottomNode1[0], swapBottomNode2[0])
            $(".accordion-big")[i-1].replaceChild(cloneBottomNode2[0], swapBottomNode1[0])

        }           
    })
}
