from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama = models.CharField(max_length=255, blank=False)
    dosen = models.CharField(max_length=255, blank=False)
    jumlah_sks = models.IntegerField(blank=False)
    semester_tahun = models.CharField(max_length=255, blank=False)
    deskripsi = models.TextField(blank=True)