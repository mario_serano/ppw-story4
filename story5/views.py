from django.shortcuts import render, redirect
from story5 import models, forms

# Index
def index(request):
    matkuls = models.MataKuliah.objects.all()
    form = forms.MatkulForm()

    context = {
        "form":form,
        "matkuls":matkuls
    }
    
    return render(request, 'story5/index.html', context)

def form(request):
    if request.method == 'POST':
        form = forms.MatkulForm(request.POST)
        if(form.is_valid()):
            cd = form.cleaned_data
            new_model = models.MataKuliah(nama=cd['nama'], dosen=cd['dosen'], semester_tahun=cd['semester_tahun'], jumlah_sks=cd['jumlah_sks'], deskripsi=cd['deskripsi'])
            new_model.save()
        return redirect('/story-5/')

def matkul_detail(request, id=None):
    is_all = False
    if(id == None):
        matkuls = models.MataKuliah.objects.all()
        if (len(matkuls) == 0):
            return redirect('/story-5/')
        is_all = True
    else:
        try:
            matkuls = models.MataKuliah.objects.get(pk=id)
        except:
            return redirect('/story-5/')
    context = {
        "matkuls":matkuls,
        "is_all":is_all
    }
    return render(request, 'story5/matkul_detail.html', context)

def delete_matkul(request, id=None):
    if (request.method == 'POST'):
        models.MataKuliah.objects.get(pk=request.POST['id']).delete()
    return redirect('/story-5/matkul-detail/')