from django import forms
from story5.models import MataKuliah

class MatkulForm(forms.Form):
    nama=forms.CharField(max_length=255, label="Nama Matkul",widget=forms.TextInput(attrs={"class":"form-textfield", "placeholder":"co: Struktur Data dan Algoritma"}))
    dosen=forms.CharField(max_length=255, label="Nama Dosen", widget=forms.TextInput(attrs={"class":"form-textfield", "placeholder":"co: Pak Fariz Darari"}))
    semester_tahun=forms.CharField(max_length=255, label="Semester", widget=forms.TextInput(attrs={"class":"form-textfield", "placeholder":"co: Semester Gasal 2019/2020"}))
    jumlah_sks=forms.IntegerField(max_value=144, label="Jumlah SKS", widget=forms.NumberInput(attrs={"class":"form-integerinput", "placeholder":"co: 5"}))
    deskripsi=forms.CharField(label="Deskripsi Matkul", widget=forms.Textarea(attrs={"class":"form-textarea", "placeholder":"co: Mata Kuliah ini sangatlah asik dan dosennya cantik banget!"}))