from django.urls import path
from . import views
urlpatterns = [
    path('', views.index),
    path('add-matkul/', views.form),
    path('matkul-detail/<int:id>/', views.matkul_detail),
    path('matkul-detail/', views.matkul_detail),
    path('matkul-detail/matkul-delete/', views.delete_matkul),
    path('matkul-detail/<int:id>/matkul-delete/', views.delete_matkul),
]